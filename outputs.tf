output "eks_vpc" {
  value = aws_vpc.k8s_network
}

output "eks_subnets" {
  value = aws_subnet.subnets
}