# eks-vpc-module

Esse modulo :
* Cria uma vpc focada no provisionamento do eks
* Cria as subnets que serão usadas pelos nodes ou fargates
* Cria um ip gateway
* Cria o router a ser usado pelas subnets
* Associa o router as subnets

## Dependências


 - [terraform](https://www.terraform.io/) >= 0.14

## Usando

Crie um arquivo **terrafile.tf** na raiz do seu projeto, você pode seguir esse exemplo:

```terraform
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket  = "NOME_DO_BUCKET"
    key     = "NOME_DO_TFSTATE"
    encrypt = true
    region  = "us-east-2"
  }
}

module "eks_vpc" {
  source       = "git@gitlab.com:modules-iac/terraform-eks-vpc?ref=master"
  network_name = "eks_network"
  vpc_cidr     = "10.0.0.0/16"
  vpc_subnets = {
    Subnet01 = {
      name              = "Subnet01",
      cidr              = "10.0.0.0/20",
      availability_zone = "us-east-2a"
    }
    Subnet02 = {
      name              = "Subnet02",
      cidr              = "10.0.16.0/20",
      availability_zone = "us-east-2b"
    }
    Subnet03 = {
      name              = "Subnet03",
      cidr              = "10.0.32.0/20",
      availability_zone = "us-east-2c"
    }
  }
}

output "vpc_id" {
  value = module.eks_vpc.eks_vpc.id
}

output "vpc_subnets_id" {
  value = {
    for subnet in module.eks_vpc.eks_subnets :
    subnet.tags["Name"] => subnet.id
  }
}
```

## Inputs

| **Nome** | **Descrição** | **Tipo** | **Default** | **Requerido** |
|------|-------------|:----:|:-----:|:-----:|
| **network_name** |  Nome da vpc | string | n/a | sim |
| **vpc_cidr** |  CIDR da vpc | string | n/a | sim |
| **vpc_subnets** |  Dados das subnets | map | n/a | sim |
