variable "network_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_subnets" {
  type = map
}
