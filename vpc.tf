resource "aws_vpc" "k8s_network" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = var.network_name
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.k8s_network.id

  tags = {
    Name = "${var.network_name}-gw"
  }
}

resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.k8s_network.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "${var.network_name}-route-table"
  }
}

resource "aws_subnet" "subnets" {
  for_each          = var.vpc_subnets
  vpc_id            = aws_vpc.k8s_network.id
  cidr_block        = each.value.cidr
  availability_zone = each.value.availability_zone

  tags = {
    Name = "${var.network_name}-${each.value.name}"
  }
}

resource "aws_route_table_association" "rt_association" {
  for_each       = var.vpc_subnets
  subnet_id      = aws_subnet.subnets[each.value.name].id
  route_table_id = aws_route_table.route_table.id
}
